

/**
 * A String node. This will render it down and validate the value
 */
import BaseProp from "./BaseProp";

/**
 * A String property. This will render it down and validate the value.
 */
export default class DateProp extends BaseProp {
	constructor( node: any, name: string ) {
		super( node, name );

		this.ownDecorator = "IsDate";

		this.minDate = node[ "x-min-date" ];
		this.maxDate = node[ "x-max-date" ];

		this.propToTemplate = {
			"minDate": "MinDate",
			"maxDate": "MaxDate"
		};
	}

	/**
	 * Checks if the value is a date that's after the specified date.
	 */
	public minDate?: Date;
	/**
	 * Checks if the value is a date that's before the specified date.
	 */
	public maxDate?: Date;





	/**
	 * The template to render this type
	 */
	get prop(): string {
		return "type.date";
	}

	/**
	 * Returns a string array of needed imports
	 */
	public  imports(): string[] {return [];}


	/**
	 * Things like extra classes and enums that need to be at the top level of the module
	 */
	public headers(): string[] {return [];}
}
