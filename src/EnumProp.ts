import { readFileSync } from "fs";
import * as sys         from "lodash";

/**
 * An Enumeration node. This will render it down and validate the value
 */
import BaseProp         from "./BaseProp";
import { classify }     from "underscore.string";
import * as template    from "ejs";

/**
 * A Enumeration property. This will render it down and validate the value.
 */
export default class EnumProp extends BaseProp {
	constructor( node: any, name: string ) {
		super( node, name );

		this.ownDecorator = "IsEnum";

		this.items    = node.enum;
		this.enumType = classify( this.name );
	}

	/**
	 * The enum contents
	 */
	public items?: any[];

	/**
	 * Things like extra classes and enums that need to be at the top level of the module
	 */
	public headers(): string[] {
		const h = [];
		const t        = readFileSync( `./templates/declareEnum.ejs` );

		const rendered = template.render( t.toString(), {
			prop: this,
			sys : this.sys
		}, this.defaultTemplateOptions );

		if ( !sys.isEmpty( rendered ) ) {
			h.push( rendered );
		}

		return h;
	}

	/**
	 * The template to render this type
	 */
	get prop(): string {
		return "type.enum";
	}

}
