/**
 * A Boolean node. This will render it down and validate the value
 */
import BaseProp from "./BaseProp";

/**
 * A Boolean property. This will render it down and validate the value.
 */
export default class BooleanProp extends BaseProp {
	constructor( node: any, name: string ) {
		super( node, name );

		this.ownDecorator = "IsBoolean";
	}
	/**
	 * The list of decorators templates supported by this type
	 */
	 decs(): string[] {
		return [ ];
	}

	/**
	 * The template to render this type
	 */
	get prop(): string {
		return "type.boolean";
	}

	/**
	 * Returns a string array of needed imports
	 */
	public imports(): string[] {return [];}

	/**
	 * Gets the validators that are actually in use so that a single import can be created
	 */
	validationImports(): string[] {
		return ["IsBoolean"];
	}

	/**
	 * Things like extra classes and enums that need to be at the top level of the module
	 */
	public headers(): string[] {return [];}
}
