import * as template    from "ejs";
import { readFileSync } from "fs";
import * as sys         from "lodash";
import { isEmpty }      from "lodash";

/**
 * A String node. This will render it down and validate the value
 */
import BaseProp         from "./BaseProp";
import configMgr        from "@concorde2k/ccconfig";

const config = configMgr.config;
import { relative }     from "path";

/**
 * A String property. This will render it down and validate the value.
 */
export default class ArrayProp extends BaseProp {

	constructor( node: any, name: string, protected symbols: any, protected parentObj: any ) {
		super( node, name );

		this.ownDecorator = "IsArray";

		this.minLength   = node.minItems;
		this.maxLength   = node.maxItems;
		this.arrayUnique = node.uniqueItems;
		this.contains    = node[ "x-contains" ];
		this.notContains = node[ "x-not-contains" ];
		this.items       = "any";

		if ( node.items && node.items.$ref ) {
			const cl = symbols[ node.items.$ref ];
			if ( cl ) {
				this.class           = cl;
				this.classSourcePath = relative( `${config.outpath}${parentObj.modelPath}`, `${config.outpath}${cl[ "x-model-path" ]}` );
				this.classSourcePath = isEmpty( this.classSourcePath ) ? "." : this.classSourcePath;
				this.items           = cl[ "x-model-name" ];
			}
		} else if ( node.items && node.items.type ) {
			this.items = node.items.type;
		}

		this.propToTemplate = {
			"contains"     : "ArrayContains",
			"notContains"  : "ArrayNotContains",
			"minLength"    : "ArrayMinSize",
			"maxLength"    : "ArrayMaxSize",
			"arrayUnique"  : "ArrayUnique",
			"arrayNotEmpty": "ArrayNotEmpty"
		};
	}

	public class?: any;
	/**
	 * What the types of the array are allowed to be
	 */
	       public items?: any;
	/**
	 * Checks if array contains all values from the given array of values.
	 */
	       public contains?: any[];

	/**
	 * Checks if array does not contain any of the given values.
	 */
	public notContains?: any[];

	/**
	 * Checks if given array is not empty.
	 */
	public notEmpty?: any[];

	/**
	 * Checks if array's length is as minimal this number.
	 */
	public minLength?: number;

	/**
	 * Checks if array's length is as maximal this number.
	 */
	public maxLength?: number;
	/**
	 * Checks if all array's values are unique. Comparison for objects is reference-based.
	 */
	public arrayUnique?: boolean;

	/**
	 * The template to render this type
	 */
	get prop(): string {
		return "type.array";
	}

	/**
	 * Returns a string array of needed imports
	 */
	public imports(): string[] {
		const h: any[] = [ "" ];
		if ( this.class ) {

			const t = readFileSync( `./templates/requireClass.ejs` );

			const rendered = template.render( t.toString(), {
				prop: this,
				sys : this.sys
			}, this.defaultTemplateOptions );

			if ( !sys.isEmpty( rendered ) ) {
				h.push( rendered );
			}
		}

		return h;
	}

	/**
	 * Things like extra classes and enums that need to be at the top level of the module
	 */
	public headers(): string[] {
		return [];
	}
}
