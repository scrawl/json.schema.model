/**
 * A Integer node. This will render it down and validate the value
 */
import BaseProp from "./BaseProp";

/**
 * A Integer property. This will render it down and validate the value.
 */
export default class IntegerProp extends BaseProp {
	constructor( node: any, name: string ) {
		super( node, name );

		this.ownDecorator = "IsInt";

		this.min           = node.minimum;
		this.max           = node.maximum;
		this.isDivisibleBy = node.multipleOf;
		this.isPositive    = node[ "x-positive-number" ];
		this.isNegative    = node[ "x-negative-number" ];

		this.propToTemplate = {
			"min"          : "Min",
			"max"          : "Max",
			"isDivisibleBy": "IsDivisibleBy",
			"isPositive"   : "IsPositive",
			"isNegative"   : "IsNegative"
		};
	}

	/**
	 * Checks if the given number is greater than or equal to given number.
	 */
	public min?: number;

	/**
	 * Checks if the given number is less than or equal to given number.
	 */
	public max?: number;

	/**
	 * Checks if the value is a positive number.
	 */
	public isPositive?: boolean;

	/**
	 * IsNegative
	 */
	public isNegative?: boolean;

	/**
	 * Checks if the value is a number that's divisible by another.
	 */
	public isDivisibleBy?: number;

	/**
	 * The template to render this type
	 */
	get prop(): string {
		return "type.integer";
	}

	/**
	 * Returns a string array of needed imports
	 */
	public imports(): string[] {return [];}

	/**
	 * Gets the validators that are actually in use so that a single import can be created
	 */

	/**
	 * Things like extra classes and enums that need to be at the top level of the module
	 */
	public headers(): string[] {return [];}

}
