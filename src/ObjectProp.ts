import * as template    from "ejs";
import { readFileSync } from "fs";
import * as sys         from "lodash";

/**
 * An Enumeration node. This will render it down and validate the value
 */
import BaseProp         from "./BaseProp";
import configMgr        from "@concorde2k/ccconfig";
import { relative }     from "path";
import { isEmpty }      from "lodash";

const config = configMgr.config;

/**
 * A Enumeration property. This will render it down and validate the value.
 */
export default class ObjectProp extends BaseProp {
	constructor( node: any, name: string, protected symbols: any, protected parentObj: any ) {
		super( node, name );

		this.ownDecorator = "IsInstance";

		this.$ref = node.$ref;

		this.class = symbols[ this.$ref ];
		if ( !this.class ) {
			this.class = {};
		}

		this.classSourcePath = relative( `${config.outpath}${parentObj.modelPath}`, `${config.outpath}${this.class[ "x-model-path" ]}` );
		this.classSourcePath = isEmpty( this.classSourcePath ) ? "." : this.classSourcePath;

	}

	/**
	 * Things like extra classes and enums that need to be at the top level of the module
	 */
	public imports(): string[] {
		const h: any[] = [ "" ];
		const t        = readFileSync( `./templates/requireClass.ejs` );

		const rendered = template.render( t.toString(), {
			prop: this,
			sys : this.sys
		}, this.defaultTemplateOptions );

		if ( !sys.isEmpty( rendered ) ) {
			h.push( rendered );
		}

		return h;
	}

	/**
	 * The template to render this type
	 */
	get prop(): string {
		return "type.object";
	}

}
