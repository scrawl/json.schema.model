import configMgr                                    from "@concorde2k/ccconfig";
import { each, isEmpty, isObject, isString, merge } from "lodash";
import { classify, camelize }                       from "underscore.string";
import { dirname }                                  from "path";

const RefParser = require( "json-schema-ref-parser" );
const config    = configMgr.config;

/**
 * Controls the parsing process and populates the symbols parameter with what it finds
 *
 * @param fileName
 * @param symbols
 */
export default async function parseFile( fileName: string, symbols: any ) {
	return new Promise( ( resolve, reject ) => {
		try {
			const parser = new RefParser();

			parser.bundle( fileName, ( err: any | null, schema: any ) => {
				if ( err ) {
					reject( err );
				}

				let toProcess = {} as any;

				if ( isString( schema.swagger ) && !isEmpty( schema.swagger ) ) {
					toProcess.definitions = schema.definitions;
					toProcess.components  = schema.components;
				} else {
					if ( isString( schema.type ) && schema.type === "object" ) {
						toProcess[ config.rootName ] = schema;
					} else {
						merge( toProcess, schema );
					}
				}

				assignKeys( toProcess, "", "#", symbols );
				resolve( symbols );
			} );

		} catch ( e ) {
			reject( e );
		}

	} );
}

function assignKeys( node: any, name: string, pth: string, symbols: any ) {
	let refPath: string;
	if ( isEmpty( name ) ) {
		refPath = `${pth}`;
	} else {
		refPath = `${pth}/${name}`;
	}

	if ( isObject( node ) ) {

		if ( node.type === "object" ) {
			symbols[ refPath ]              = node;
			node[ "x-ref-path" ]            = refPath;
			node[ "x-model-path" ]          = dirname(node[ "x-model-path" ] || refPath.replace( /properties\//g, "" ).replace( "#/", "" ));
			node[ "x-model-name" ]          = node[ "x-model-name" ] || classify( name );
			node[ "x-model-instance-name" ] = node[ "x-model-instance-name" ] || camelize( node[ "x-model-path" ].replace( "#/", "" ).replace( /\//g, "-" ) );
			node[ "x-namespace" ]           = node[ "x-model-path" ].replace( /\//g, "." );
		}

		each( node, ( nextNode: any, nextKey: string ) => {
			assignKeys( nextNode, nextKey, refPath, symbols );
		} );

	}
}
