/**
 * Creates decorated objects from a json-schema/swagger document
 */

process.env.DEBUG = process.env.DEBUG ? `$process.env.DEBUG} schemaModel:*` : "schemaModel:*";
import configMgr                    from "@concorde2k/ccconfig";
import * as glob                    from "glob";
import { each }                     from "async";
import { each as eachNode, extend } from "lodash";
import { getLogger }                from "@concorde2k/cclogger";

configMgr.cli.option(
	"f",
	{
		alias       : "infiles",
		demandOption: true,
		describe    : "The path to your schema files, can be a glob to yaml or json files or a mix thereof",
		type        : "string"
	}
);

configMgr.cli.option(
	"o",
	{
		alias       : "outpath",
		demandOption: true,
		describe    : "The path to write the files to",
		type        : "string"
	}
);

configMgr.cli.option(
	"r",
	{
		alias       : "rootName",
		demandOption: false,
		default     : "root",
		describe    : `When dealing with a schema that does not contains a root element, you can
			name the resultant object with this. This is only valuable when you
			generating from a single file.`,
		type        : "string"
	}
);

const logger = getLogger( "schemaModel" );

configMgr.load();
const config = configMgr.config;

import parseFile    from "./parseFile";
import ObjectParser from "./ObjectParser";

logger.info( `Globbing ${config.infiles}` );
const files   = glob.sync( config.infiles );
const symbols = {};

// roll through each file
each( files, async ( v: string, done: Function ) => {

	logger.info( `found ${v}` );

	extend( symbols, await parseFile( v, symbols ) );

	done();
}, () => {

	const obj: any = [];

	eachNode( symbols, ( v, k ) => {
		let op = new ObjectParser( v, symbols );
		op.parse();
		obj.push( op );
	} );

	eachNode( obj, ( v, k ) => {
		v.render();
	} );

	process.nextTick( () => {
		logger.info( `Done writing ${files.length} files to ${config.outpath}` );
	} );

} );

