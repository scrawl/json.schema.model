# JSON Schema Model 

Generates TypeScript decorated with [class-validator](https://github.com/typestack/class-validator) decorators based on a json-schema.

## State of Development
Like many other open-source projects that are understaffed, this is somewhere on the continuum
between "good enough to get out the door" and "all of the planned and dreamed for features are there".
In this case that is further exacerbated by the fact that json-schema seems to have devolved into a 
a few different dialects. 

This project was envisioned as a way of gaining access to strongly typed definitions/components from
Swagger, with pure json-schema a nice-to-have. But some differences from Swagger to json-schema were difficult to reconcile.
Finally, class-validator accepts json-schema in its validation tools, but the decorators
are a disjoint language against Swagger and json-schema. 

Which might beg the question: why do this? If you aren't asking that question, you aren't thinking 
carefully about what this project does. Well one answer is that this project makes sense if
you consider json-schema/Swagger as a means of validating serialized objects in transit or at rest
and that there is a role for the object that contains the hydrated instance to continue the 
original schema's validation as well as the values it mutates along its lifetime. Indeed, this
library does not try to connect the lifetimes of the JSON and the object in any way. If you use
Swagger, the swagger-tools will pull these objects out of `definitions` and `paths` and return you a
plain old JS object. You can pass that into one of these constructors and, since it is validated, serialize
it again knowing it has to be valid. Anyway, that's the idea.

# Installation
```bash
npm install @scrawl/json.schema.model -g

```

#CLI

This is a static code generator, there is no API, just a CLI. The options are:
* `--help`          Show help
* `--version`       Show version number
* `--log-level`     Sets the logging level for the process. Choices are
                    "trace", "debug", "warn", "data", "log", "info", "warn", "error"
* `-f, --infiles`   The path to your schema files, can be a glob to yaml or json
                     files or a mix thereof
* `-o, --outpath`   The path to write the files to. Class paths are appended to this
* `-r, --rootName`  When dealing with a schema that does not contains a root element, you can
                    name the resultant object with this. This is only valuable when you 
                    generating from a single file.
                    
# Validation keywords
`json.schema.model` supports the common subset of keywords from swagger and jon-schema. Where the interpretations are
in conflict, this tends to prefer swagger.

## Common keywords
* `name` This is specific to `json.schema.model` and is used to name a class from the key of the object definition. it is autopopulated 
and is available in the template that generates the code. 
```js
{
    "MyModel": { // <-- this is the name property of the runtime generator
        "type": "object",
        "properties":{}
    }
}
```
* `title:string` as in json-schema
* `description:string` as in json-schema
* `x-is-defined:boolean` Extension for class-validator, checks if value is defined (!== undefined, !== null)
* `required:array` as in json-schema
* `const:string` as in json-schema. Also as `x-equals`. Checks if value equals ("===") comparison.
* `x-not-equals` as in class-validator. Checks if value not equal ("!==") comparison.
* `x-empty:boolean` as in class-validator. Checks if given value is empty (=== '', === null, === undefined).
* `x-not-empty:boolean` as in class-validator. Checks if given value is not empty (!== '', !== null, !== undefined).
* `x-in:array` as in class-validator. Checks if value is in a array of allowed values.
* `x-not-in:array` as in class-validator. Checks if value is not in a array of disallowed values.
* `x-default` specify a default value for the property
* `string`, `number`, `integer`, `boolean`, `array`, `object` data type keywords as in json-schema

## Object keywords
* `x-model-name:string` specify a name for the generated class
* `x-model-path:string` specify a path (starting from `outFiles` option) that the module should be written
* `x-namespace:string` Used for documentation only when defining the `@module` keyword
* `x-extends:string` If this is a subclass of some other class, put that class path here

## Array keywords
* `minItems`, `maxItems`, `uniqueItems` as in json-schema
* `x-contains:array` as in class-validator. Checks if array contains all values from the given array of values.
* `x-not-contains:array` as in class-validator. Checks if array does not contain any of the given values.
* `items:array` as in swagger which is always an array

## Date keywords
* `x-min-date:Date|string` as in class-validator. Checks if the value is a date that's after the specified date.
* `x-max-date:Date|string` as in class-validator. Checks if the value is a date that's before the specified date.

## Integer/Number keywords
* `minimum`, `maximum`, `multipleOf` as json-schema
* `x-positive-number:boolean` as in class-validator. Checks if the value is a positive number.
* `x-negative-number:boolean` as in class-validator. Checks if the value is a negative number.

## String keywords
* `minLength`, `maxLength`, `pattern` as in json-schema
* `x-contains:string` as in class-validator. Checks if the string contains the seed.
* `x-not-contains` as in class-validator. Checks if the string not contains the seed.


                 
                      





     


