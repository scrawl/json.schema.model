
FIND := /usr/bin/find
DOCS-TOOL := typedoc
VERSION-TYPE := patch
VERSION-PREFIX := alpha

OUT-DIR := dist
SRC-DIR := src

DOCS-OUT := docs

SOURCE-TS-FILES := $(shell ${FIND} ${SRC-DIR}/ -type f -name "*.ts")
SOURCE-TS-FILES-OUT = $(SOURCE-TS-FILES:.ts=.js)
SOURCE-TS-FILES-OUT := $(subst ${SRC-DIR}/,${OUT-DIR}/,${SOURCE-TS-FILES-OUT})

EJS-FILES := $(shell ${FIND} ${SRC-DIR}/ -type f -name "*.ejs")
EJS-FILES-OUT = $(EJS-FILES:.EJS=.EJS)
EJS-FILES-OUT := $(subst ${SRC-DIR}/,${OUT-DIR}/,${EJS-FILES-OUT})

PACKAGEJSON-FILE := package.json
README-FILE := README.md
TYPEDOC-CONFIG-FILE := typedoc.js
MAIN-FILE := index.ts

build: node_modules ${EJS-FILES-OUT} ${SOURCE-TS-FILES-OUT} ${OUT-DIR}/${PACKAGEJSON-FILE} makefile

typings: ${SRC-DIR}/${TYPEING-FILE} ${OUT-DIR}/${TYPEING-FILE}


${OUT-DIR}/templates/%.ejs: ${SRC-DIR}/templates/%.ejs
	mkdir -p $(@D)
	cp $< $@

techdocs: ${DOCS-OUT}

build-all: clean build ${DOCS-OUT}

${OUT-DIR}/%.js: ${SRC-DIR}/%.ts
	mkdir -p $(@D)
	tsc --target es2015 --lib 'ES2015' --module 'commonjs' --sourceMap --outDir $(@D) $<

${OUT-DIR}/${PACKAGEJSON-FILE}: ./${PACKAGEJSON-FILE}
	@cp ./${PACKAGEJSON-FILE} ${OUT-DIR}/${PACKAGEJSON-FILE}

${OUT-DIR}/${README-FILE}: ./${README-FILE}
	@cp ./${README-FILE} ${OUT-DIR}/${README-FILE}

${DOCS-OUT}: ${SOURCE-TS-FILES} makefile ${README-FILE} ${PACKAGEJSON-FILE} ${TYPEDOC-CONFIG-FILE}
	@${DOCS-TOOL} --out ${DOCS-OUT} --options ./typedoc.js ${SRC-DIR}
	@touch -m ${DOCS-OUT}

node_modules: package.json
	@npm install
	@touch -m node_modules

clean:
	@rm -Rf ${OUT-DIR}/*

dist-clean: clean
	@rm -Rf node_modules*
	@rm -Rf ${DOCS-OUT}*

publish: build ${OUT-DIR}/${README-FILE}
	@cp ./${README-FILE} ${OUT-DIR}/${README-FILE}
	@cp ./${PACKAGEJSON-FILE} ${OUT-DIR}/${PACKAGEJSON-FILE}
	@cd dist && \
	npm publish --access=public

version-up:
	@echo Updating version number
	@-node -e "let v = require('./package.json');v.version = require('semver').inc(v.version, '${VERSION-TYPE}', '${VERSION-PREFIX}');let fs = require('fs');fs.writeFileSync('./package.json', JSON.stringify(v, null, 2));"
	@echo Checking changes in
	@-git commit -a -m "Creating version ${shell node -p "require('./package.json').version;"}"
	@echo Tagging
	@git tag -a v${shell node -p "require('./package.json').version;"} -m "v${shell node -p "require('./package.json').version;"}"
	@echo Pushing
	@git push

release: build-all version-up publish

watch:
	@tsc --watch
